
import toxnetCore.Capability as Capability

class DisplayCapability(Capability.Capability):

	def __init__(self):
		Capability.Capability.__init__(self)
		self.setName("Text Display")
		self.setDescription("Display Text. Parameter: message <String>")
		pass


	def execute(self,message):
		value = None
		try:
			value = message["parameter"]
		except Exception, e:
			print ("Not a valid JSON message: " + str(message) + ": " + str(e))
			return None

		
		print(value)
		return None

