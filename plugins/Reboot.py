
import toxnetCore.Capability as Capability
import subprocess

class RebootCapability(Capability.Capability):

	def __init__(self):
		Capability.Capability.__init__(self)
		self.setName("Reboot")
		self.setDescription("Reboot the target machine. Parameter: none")
		pass


	def execute(self,message):
		command = "/usr/bin/sudo /sbin/shutdown -r now"
		process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
		output = process.communicate()[0]
		print output
		return None



