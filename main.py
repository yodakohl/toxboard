#A minimal toxnet client
import sys
from toxnetCore import *

client = ToxnetCore(sys.argv[1],"/usr/bin/Client/plugins")
client.setName("minimal Client")
client.friend_add("D39704964B3B39B0D2FBCD50996345CC06FD7EB7E1127F1A9B3FC6F77DB2C078BA7366B0BC0D","Update Request")
client.friend_add("94C707EE0701CF8C93F46E666EC579C41F0DC840C5C08996A89259559E212945721FE2EBFC7B","Update Request")
#TODO Add trusted main node as friend

client.run()
