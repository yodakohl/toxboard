#!/bin/sh

echo "Starting Firmware Update"
mkdir /recovery
mount /dev/mmcblk0p4 /recovery
echo "Mountet Recovery Image"

echo "Createt update Files"
cd /recovery
mkdir oldroot
mkdir update

echo "Pivoting root"
pivot_root . oldroot
echo "Finished Pivot"

echo "Writing Image"
mount oldroot/dev/mmcblk0p3 ./update
dd bs=1M if=./update/image.bin of=oldroot/dev/mmcblk0p2
sync

echo "Done, rebooting"
reboot -f
